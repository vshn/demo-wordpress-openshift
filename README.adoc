= WordPress on OpenShift

This project contains the required manifests to install WordPress and MariaDB on an OpenShift cluster.

== How to Use

Connect to your OpenShift cluster and then:

[source,bash]
--
$ oc apply -f mariadb
$ oc apply -f wordpress
--

And then open https://wordpress-openshift.apps.exoscale-ch-gva-2-0.appuio.cloud/ in your browser to finish the installation.

== Docker Image

The standard WordPress image cannot run on an OpenShift cluster, since it is based on the standard Apache image, exposing port 80. The `docker` folder contains a Dockerfile which can be used to create a suitable image that does run on OpenShift, exposing port 8080 instead.
